﻿namespace CompositionChallenge1
{
    interface ISomeHelperInterface
    {
        string OuterMethod();
    }

    class SomeHelperClassA : ISomeHelperInterface
    {
        public string OuterMethod()
        {
//            var innerText = InnerMethod();
            var innerText = "GetMyInnerMethod";
            return "SomeHelperA and " + innerText;
            // We want different implementations of this method separate to different implentations of outer method.
        }
    }

    abstract class SomeBasePage: ISomeHelperInterface
    {
        private ISomeHelperInterface _someHelperImplementation;

        protected SomeBasePage()
        {
            _someHelperImplementation = new SomeHelperClassA();
        }

        // Delegated Methods
        public string OuterMethod()
        {
            return _someHelperImplementation.OuterMethod();
        }
    }

    class SomePageFromBase : SomeBasePage
    {
        // Wants to define own version of 'InnerMethod'
    }

    class SomeOtherPageNotFromBase : ISomeHelperInterface
    {
        private ISomeHelperInterface _someHelperImplementation;

        public SomeOtherPageNotFromBase()
        {
            _someHelperImplementation = new SomeHelperClassA();
            // Wants this outermethod implementation but different innermethod implementation.
        }

        // Delegated Methods
        public string OuterMethod()
        {
            return _someHelperImplementation.OuterMethod();
        }
    }
}
