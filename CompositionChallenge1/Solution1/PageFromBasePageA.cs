﻿namespace CompositionChallenge1.Solution1
{
    // Wants the utilities in BasePage but with own custom InnerMethod()
    internal class PageFromBasePageA : BasePage
    {
        private static readonly IInnerMethodInterface InnerMethodInterface 
            = new CustomInnerMethod1();

        public PageFromBasePageA() : base(InnerMethodInterface)
        {            
        }
    }

    internal class CustomInnerMethod1 : IInnerMethodInterface
    {
        public string InnerMethod()
        {
            return "PageFromBasePageA specific custom InnerMethod";
        }
    }
}
