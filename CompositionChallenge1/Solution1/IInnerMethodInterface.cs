﻿namespace CompositionChallenge1.Solution1
{
    internal interface IInnerMethodInterface
    {
        string InnerMethod();
    }

    internal class GenericInnerMethodImplementation : IInnerMethodInterface
    {
        public string InnerMethod()
        {
            return "Generic Inner Method used";
        }
    }
}
