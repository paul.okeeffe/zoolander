﻿namespace CompositionChallenge1.Solution1
{
    internal interface ISomeHelper
    {       
        string OuterMethod();
    }

    internal class SomeHelperA : ISomeHelper
    {
        private readonly IInnerMethodInterface _innerMethodImplementation;

        public SomeHelperA(IInnerMethodInterface innerMethodInterface)
        {
            _innerMethodImplementation = innerMethodInterface;
        }

        public string OuterMethod()
        {
            var innerText = _innerMethodImplementation.InnerMethod();
            return "SomeHelperA and " + innerText;
            // We want different implementations of this method separate to different implentations of outer method.
        }
    }
}
