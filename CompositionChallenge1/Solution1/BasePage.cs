﻿namespace CompositionChallenge1.Solution1
{
    internal abstract class BasePage : ISomeHelper
    {
        private readonly ISomeHelper _someHelperImplementation;

        protected BasePage(IInnerMethodInterface innerMethodImplementation)
        {
            _someHelperImplementation = new SomeHelperA(innerMethodImplementation);
        }

        // Delegated Methods - hooray, freebies for composition
        public string OuterMethod()
        {
            return _someHelperImplementation.OuterMethod();
        }
    }
}
