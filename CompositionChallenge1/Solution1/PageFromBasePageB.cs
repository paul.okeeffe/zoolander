﻿namespace CompositionChallenge1.Solution1
{
    // Wants the utilities in BasePage without custom InnerMethod()
    internal class PageFromBasePageB : BasePage
    {
        private static readonly IInnerMethodInterface InnerMethodInterface
            = new GenericInnerMethodImplementation();

        public PageFromBasePageB() : base(InnerMethodInterface)
        {
        }
    }

}
