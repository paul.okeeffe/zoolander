﻿namespace CompositionChallenge1.Solution1
{
    internal class OtherPageNotFromBase : ISomeHelper
    {
        private readonly ISomeHelper _someHelperImplementation;
        private readonly IInnerMethodInterface _innerMethodInterface;

        public OtherPageNotFromBase()
        {
            _innerMethodInterface = new CustomOtherPageInnerMethod();
            _someHelperImplementation = new SomeHelperA(_innerMethodInterface);
        }

        // Delegated Methods
        public string OuterMethod()
        {
            return _someHelperImplementation.OuterMethod();
        }
    }

    internal class CustomOtherPageInnerMethod : IInnerMethodInterface
    {
        public string InnerMethod()
        {
            return "OtherPageNotFromBase specific custom InnerMethod"; ;
        }
    }
}
