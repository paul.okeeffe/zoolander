﻿using System;
using CompositionChallenge1.Solution1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompositionChallenge1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestComposition1()
        {
            /** How to compose a interfance and implementation such that it can be
             * a) mixed in to other classes for reuse
             * b) depend on different implemetations of an inner called method
             */

            var pageAText = new PageFromBasePageA().OuterMethod();
            Console.WriteLine("PageFromBasePageA 'custom' example: " + pageAText);
            Assert.IsTrue(pageAText.ToLower().Contains("custom"));

            var pageBText = new PageFromBasePageB().OuterMethod();
            Console.WriteLine("PageFromBasePageB 'generic' example: " + pageBText);
            Assert.IsTrue(pageBText.ToLower().Contains("generic"));

            var otherPageText = new OtherPageNotFromBase().OuterMethod();
            Console.WriteLine("OtherPageNotFromBasePage 'custom' example: " + otherPageText);
            Assert.IsTrue(otherPageText.ToLower().Contains("custom"));
        }
    }
}
