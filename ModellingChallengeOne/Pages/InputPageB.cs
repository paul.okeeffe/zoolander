﻿using ModellingChallengeOne.UI;
using OpenQA.Selenium;

namespace ModellingChallengeOne.Pages
{
    class InputPageB : BasePage
    {
        public InputPageB(IWebDriver driver) : base(driver)
        {
        }

        internal override void SwitchToFrame()
        {
            this.SwitchToBaseFrame();
            SwitchToSubFrame("wizardFrame");
        }

        public void ClickSaveInTopBar()
        {
            SwitchToFrame();
            Driver.FindElement(By.ClassName("headerbarWiz"))
                .FindElement(By.LinkText("Save")).Click();
        }

        public FixedInputArea GetInputArea()
        {
            SwitchToFrame();
            return new FixedInputArea(Driver.FindElements(By.ClassName("standard fixed inpput area"))[1]);
        }
    }
}