﻿using ModellingChallengeOne.UI;
using OpenQA.Selenium;

namespace ModellingChallengeOne.Pages
{
    class InputPageA : BasePage
    {
        public InputPageA(IWebDriver driver) : base(driver)
        {
        }

        internal override void SwitchToFrame()
        {
            this.SwitchToBaseFrame();
            SwitchToSubFrame("wizardFrame");
        }

        public void ClickSaveInTopBar()
        {
            SwitchToFrame();
            Driver.FindElement(By.ClassName("headerbarWiz"))
                .FindElement(By.LinkText("Save")).Click();
        }

        public FixedInputAreaSubTypeA GetInputArea()
        {
            //THIS IS SLOW IF WE DO IT EVERYTIME WITHOUT CHECKING IF ALREADY SWITCHED!
            SwitchToFrame();
            // two reports regions here, one is wizard title and top buttons, second is main input area.
            return new FixedInputAreaSubTypeA(Driver.FindElements(By.ClassName("reports_region_th"))[1]);
        }
    }
}