﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace ModellingChallengeOne.Pages
{
    public abstract class BasePage
    {
        public string WindowHandle { get; private set; }

        protected IWebDriver Driver { get; private set; }

        protected IWebElement HtmlElement { get; private set; }

        private WebDriverWait _webDriverWait {  get;  set; }

        private TimeSpan _timeout = TimeSpan.FromSeconds(10); // Would go in an environment config file
        

        protected BasePage(IWebDriver driver)
        {
            this.Driver = driver;
            _webDriverWait = new WebDriverWait(Driver, _timeout);
            WindowHandle = driver.CurrentWindowHandle;
            SwitchToFrame();
        }

        protected void CaptureHtmlElement()
        {
            HtmlElement = Driver.FindElement(By.TagName("html"));
        }

        /*
         * checks for staleness of the HTML element to know when the page has reloaded
         */

        public void WaitForPageLoad()
        {
            try
            {
                _webDriverWait.Until(ExpectedConditions.StalenessOf(HtmlElement));
            }
            catch (Exception) { }

            CaptureHtmlElement();
        }

        public void WaitForElementReLoad(IWebElement elementThatChanges)
        {
            try
            {
                _webDriverWait .Until(ExpectedConditions.StalenessOf(elementThatChanges));
            }
            catch (Exception) { }
        }

/*
         * switches to the correct window and frame
         */

        public void SetDriverContext()
        {
            Driver.SwitchTo().Window(this.WindowHandle);
            this.SwitchToFrame();
        }

        /*
         * All pages in this app start with this frame context
         */
        protected void SwitchToBaseFrame()
        {
            Driver.SwitchTo().DefaultContent();
            Driver.SwitchTo().Frame(0);
            CaptureHtmlElement();
        }

        /*
         * Some pages in app have additional levels of nested frames, therefore this method should 
         * be overridden - the overriding method should call base.SwitchToFrame() before switching to 
         * the nested frame
         */
        internal virtual void SwitchToFrame()
        {
            this.SwitchToBaseFrame();
        }

        // special method for switching to subframes as a workaround is required for chrome!
        internal void SwitchToSubFrame(string frameId, bool switchToTopFrameFirst = false)
        {
            if (switchToTopFrameFirst)
            {
                SwitchToFrame();
            }

            _webDriverWait.Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt((frameId)));

            CaptureHtmlElement();

            // Issue 2198 reported for Chrome and iframe content suggests wait as a workaround..
//            if (TestEnvironment.Browser.Equals(BrowserType.Chrome))
//            {
//                Thread.Sleep(4000);
//            }
        }
    }
}