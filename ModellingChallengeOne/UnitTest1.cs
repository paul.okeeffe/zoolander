using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModellingChallengeOne.Pages;
using ModellingChallengeOne.UI;
using OpenQA.Selenium.Chrome;

namespace ModellingChallengeOne
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void FixPageTestModelOverlap()
        {
            var driver = new ChromeDriver();


            // AIM: Remove model reload handling from Test Layer


            // Undesired (BAD) - test must get model from page and decide when to wait for it to reload / new up another one.

            var inputPageA = new InputPageA(driver);
      
            FixedInputAreaSubTypeA fixedInputAreaSubTypeA = inputPageA.GetInputArea()
                .SelectTextByPartialMatch("fieldName", "fieldValue"); // reloads the wizardFrame containing the area element
            inputPageA.WaitForElementReLoad(fixedInputAreaSubTypeA.Element);
            fixedInputAreaSubTypeA = inputPageA.GetInputArea()
                .SetText("fieldName", "fieldValue");
            fixedInputAreaSubTypeA.ClickSpecialUnlabelledIconA();

            // Happy for test layer to determine when we are on a new page.
            var inputPageB = new InputPageB(driver);

            
            // Desired Model Behaviour     
            
            inputPageA = new InputPageA(driver);
            inputPageA
                .SelectTextByPartialMatch("inputAreaIdentifier","fieldName", "fieldValue") // page model detects reload - may only apply to certain field names - news up input area
                .SetText("inputAreaIdentifier", "fieldName", "fieldValue");
            inputPageA
                .ClickSpecialUnlabelledIconA("inputAreaIdentifier");

            // Happy for test layer to determine when we are on a new page.
            inputPageB = new InputPageB(driver);
        }
    }
}
