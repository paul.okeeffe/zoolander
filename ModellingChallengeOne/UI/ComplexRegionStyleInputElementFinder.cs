﻿using OpenQA.Selenium;

namespace ModellingChallengeOne.UI
{
    public class ComplexRegionStyleInputElementFinder :IInputElementFinder
    {
        public IWebElement FindInputElementByLabelText(MInputElementsHelper self, string fieldLabel)
        {
            // Input element finder logic here
            return self.Element.FindElement(By.CssSelector("some complex pattern"));
        }

        public IWebElement FindRadioElementByLabelText(MInputElementsHelper self, string fieldLabel, string text)
        {
            // Radio Input element finder logic here
            IWebElement containerElement = self.Finder.FindInputElementByLabelText(self, fieldLabel);
            return containerElement.FindElement(By.CssSelector("some complex pattern"));
        }
    }
}
