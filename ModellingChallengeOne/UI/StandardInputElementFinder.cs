﻿using ModellingChallengeOne.UI;
using OpenQA.Selenium;

namespace Hpw.Automation.Reside.UI
{
    public class StandardInputElementFinder : IInputElementFinder
    {
        public IWebElement FindInputElementByLabelText(MInputElementsHelper self, string fieldLabel)
        {
            // Input element finder logic here
            return self.Element.FindElement(By.CssSelector("some pattern"));
        }

        public IWebElement FindRadioElementByLabelText(MInputElementsHelper self, string fieldLabel, string text)
        {
            // Radio Input element finder logic here
            IWebElement containerElement = self.Finder.FindInputElementByLabelText(self, fieldLabel);
            return containerElement.FindElement(By.CssSelector("some pattern"));
        }
    }
}