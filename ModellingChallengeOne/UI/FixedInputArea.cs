﻿using OpenQA.Selenium;

namespace ModellingChallengeOne.UI
{
    // Unlike other areas this is not collaspable / expandable, although they share similar identifier patterns for elements.
    public class FixedInputArea : BaseFixedInputArea<FixedInputArea>
    {
        protected internal FixedInputArea(IWebElement maintainTemplateElement) : base(maintainTemplateElement)
        {
        }
    }
}
