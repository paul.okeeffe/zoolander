﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Internal;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace ModellingChallengeOne.UI
{
    /**  Extends BaseFixedInputArea as we need access to unlabelled input fields that dynamically display depending on a radio option
     * - difficult to generalise code to support all possible cases.
     */
    public class FixedInputAreaSubTypeA : BaseFixedInputArea<FixedInputAreaSubTypeA>
    {
        private readonly WebDriverWait _webDriverWait;
        private readonly TimeSpan _timeout = TimeSpan.FromSeconds(10); // Would go in an environment config file

        protected internal FixedInputAreaSubTypeA(IWebElement inputAreaElement) : base(inputAreaElement)
        {
            _webDriverWait = new WebDriverWait(((IWrapsDriver) Element).WrappedDriver, _timeout);
            Finder = new ComplexRegionStyleInputElementFinder(); // override base class Finder
        }

        // supports specific unlabelled field.
        public void ClickSpecialUnlabelledIconA()
        {
            _webDriverWait.Until(ExpectedConditions.ElementToBeClickable(
                Element.FindElement(By.Id("elementIdA")))).Click();                        
            // opens another orphaned page. 
        }

        public void ClickSpecialUnlabelledIconB()
        {
            _webDriverWait.Until(ExpectedConditions.ElementToBeClickable(
                Element.FindElement(By.Id("elementIdB")))).Click();
            // opens another orphaned page. 
        }
    }
}
