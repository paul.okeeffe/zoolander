﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ModellingChallengeOne.UI
{
    public interface MInputElementsHelper
    {
        IWebElement Element { get; } // outermost element from which all inputs are located within.
        IInputElementFinder Finder { get; } // Determines element locating strategy to use
    }

    internal static class InputElementsHelper
    {
        public static T ClickRadioOption<T>(this MInputElementsHelper self, string fieldName, string text)
        {
          self.Finder.FindRadioElementByLabelText(self, fieldName, text).Click();
          return (T) self;
        }

        public static T SetText<T>(this MInputElementsHelper self, string fieldName, string text, bool withTabOut = false)
        {
            IWebElement element = self.Finder.FindInputElementByLabelText(self, fieldName);
            element.Clear();
            element.SendKeys(text);
            if (withTabOut)
            {
                element.SendKeys(Keys.Tab);
            }
            return (T) self;
        }

        public static T SelectTextByPartialMatch<T>(this MInputElementsHelper self, string fieldName, string text)
        {
            new SelectElement(self.Finder.FindInputElementByLabelText(self, fieldName))
                .SelectByText(text, true); // Note requires selenium 3.11.1 and higher  - fix to issue #3575
            return (T) self;
        }

        public static T SelectTextByExactMatch<T>(this MInputElementsHelper self, string fieldName, string text)
        {
            new SelectElement(self.Finder.FindInputElementByLabelText(self, fieldName))
                .SelectByText(text, false); // Note requires selenium 3.11.1 and higher  - fix to issue #3575
            return (T) self;
        }

        public static T SelectRandomOption<T>(this MInputElementsHelper self, string fieldLabel)
        {
            SelectElement selectElement = new SelectElement(self.Finder.FindInputElementByLabelText(self, fieldLabel));
            int random = new Random().Next(1, selectElement.Options.Count); // Assumes first option is blank or guidance text eg 'Please Select.'
            selectElement.SelectByIndex(random);
            return (T) self;
        }

        public static string GetValueFromField(this MInputElementsHelper self, string fieldLabel)
        {
            return self.Finder.FindInputElementByLabelText(self, fieldLabel).GetAttribute("value");
        }
    }
}
