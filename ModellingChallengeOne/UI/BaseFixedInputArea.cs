﻿using System.Linq;
using Hpw.Automation.Reside.UI;
using OpenQA.Selenium;

namespace ModellingChallengeOne.UI
{
    /** DESIGN CONSIDERATION:
 * Represents various input areas that open up inside frames /pages
 * 'CreateServiceRequestArea'  at top of ClientViewPage - is a specific example.
 * Consider whether to keep this and speed development/ reuse with  component abstraction for UI.
 * Mostly performs well but there are different locator methods required that may slow / complicate future development,
 * may be better to have all specifically named input areas and hard-coded IDs for elements, for maintainability in new system.
 * */

    public abstract class BaseFixedInputArea<T> : MInputElementsHelper
    {
        public IWebElement Element { get; }
        public IInputElementFinder Finder { get; set;}  

        protected internal BaseFixedInputArea(IWebElement inputAreaElement)
        {
            Element = inputAreaElement;
            Finder = new StandardInputElementFinder(); // may be overriden by subclasses eg FixedInputAreaSubTypeA
        }

        public void ClickButton(string buttonText)
        {
            Element.FindElements(By.CssSelector("input[type='button']"))
                .First(element => element.GetAttribute("value").Equals(buttonText)).Click();
        }

        public T ClickRadioOption(string fieldLabel, string optionText)
        {
            return this.ClickRadioOption<T>(fieldLabel, optionText);
        }

        public T SelectRandomOption(string fieldLabel)
        {
            return this.SelectRandomOption<T>(fieldLabel);
        }

        public T SelectTextByPartialMatch(string fieldLabel, string text)
        {
            return this.SelectTextByPartialMatch<T>(fieldLabel, text);
        }

        public T SetText(string fieldLabel, string text, bool withTabOut = false)
        {
            return this.SetText<T>(fieldLabel, text, withTabOut);       
        }      
    }
}
