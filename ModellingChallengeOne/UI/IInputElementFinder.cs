﻿using OpenQA.Selenium;

namespace ModellingChallengeOne.UI
{
    public interface IInputElementFinder
    {
        IWebElement FindInputElementByLabelText(MInputElementsHelper self, string fieldLabel);

        IWebElement FindRadioElementByLabelText(MInputElementsHelper self, string fieldLabel, string text);
    }
}
